Source: r-bioc-fishpond
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Steffen Moeller <moeller@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-fishpond
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-fishpond.git
Homepage: https://bioconductor.org/packages/fishpond/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-abind,
               r-cran-gtools,
               r-bioc-qvalue,
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-bioc-summarizedexperiment,
               r-bioc-genomicranges,
               r-cran-matrixstats,
               r-cran-svmisc,
               r-cran-matrix,
               r-bioc-singlecellexperiment,
               r-cran-jsonlite
Testsuite: autopkgtest-pkg-r

Package: r-bioc-fishpond
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: Differential transcript and gene expression with inferential replicates
 Fishpond contains methods for differential transcript
 and gene expression analysis of RNA-seq data using inferential
 replicates for uncertainty of abundance quantification,
 as generated by Gibbs sampling or bootstrap sampling. Also the
 package contains utilities for working with Salmon and Alevin
 quantification files.
